""" 
Implementation of Stack Data Structure in Python.

A stack is an abstract data type that follows the LIFO
or Last In First Out operations. Below is the implementation
of the stack data type with the following methods:

1. Check if the stack is empty on not
2. Add an item in the stack
3. Return the top item in the stack
4. Check the length/size of the stack
5. Remove item top of the stack

"""

class Stack:

    def __init__(self):
        self.stack = []


    def is_empty(self):
        return self.stack == []


    def add_to_stack(self, item):
        self.stack.append(item)


    def top_of_stack(self):
        return self.stack[-1]

    def pop_stack(self):
        self.stack.pop()

    
    def size(self):
        if self.stack:
            return len(self.stack)
        else:
            return 0

    def __str__(self):
        if self.stack:
            return " \n".join([item for item in self.stack[::-1]])
        else:
            return None



# Sample Implementation

my_stack = Stack()
print(my_stack.size())

my_stack.add_to_stack("C++")
my_stack.add_to_stack("Python")
my_stack.add_to_stack("JavaScript")
print(my_stack.size())

# Return Top of Stack
print(my_stack.top_of_stack())

# Remove top of stack item
my_stack.pop_stack()
print(my_stack) 